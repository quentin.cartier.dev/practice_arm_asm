# Hello, world in assembly arm

```
arm-linux-gnueabi-as hello_world.asm -o hello_world.o
# Using static linkage and no standard library
arm-linux-gnueabi-gcc-10 hello_world.o -o hello_world.elf -static -nostdlib
qemu-arm-static ./hello_world.elf 
echo $?
```