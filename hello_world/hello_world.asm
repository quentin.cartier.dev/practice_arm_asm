# ARM32 Hello world example

.global _start
.section .text

_start:

    mov r7, #0x4 //; writie syscall
    mov r0, #1   //; stdout ( fd = 1)
    ldr r1, =m_message
    mov r2, #13  //; message nb of bytes
    swi 0 ; call software interrupt

    mov r7, #0x1 //; exit syscall
    mov r0, #13  //; error code
    swi 0

.section .data
    m_message:
    .ascii "Hello, world\n"
