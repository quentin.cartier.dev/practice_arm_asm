# Read User Input
In this example, we read user input in assembly, convert the two obtained nummber, and do addition.
We assume that the user don't not provide bad input.
```
arm-linux-gnueabi-as read_user_input.asm -o read_user_input.o
# Using static linkage and no standard library
arm-linux-gnueabi-gcc-10 read_user_input.o -o read_user_input.elf -static -nostdlib
qemu-arm-static ./read_user_input.elf 
#[enter two number, pressing enter after each]
echo $?
```

## What is learned

### User input
Retrieving user input and storing it in a buffer
### Converting a ascii string to integer
We retrieve for user a number as a ascii string



## More 

### SVC (SuperVisor Call)
This instruction causes an exception.
- Was called SWI in earlier versions of ARM asm.
Syntax is :
```
SVC{cond} #imm
 ```
 *cond*: optional condition code
 *imm*: expression evaluating to an integer (0 to 2²⁴ -1)

 