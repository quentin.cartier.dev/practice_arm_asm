.section .text

.global _start

//; r0 = buffer to write to
//; r1 = number of bytes to read
read_user_input:
    push {lr} //; func prolog
    push {r4-r11}
    push {r1}
    push {r0}
    mov r7, #0x3 //; read syscall
    mov r0, #0x0
    pop {r1} //; read buffer
    pop {r2} //; count
    svc #0x0
    pop {r4-r11} //; func epilog
    pop {pc} 

//; r0 = pointer to a string of ascii numbers
convert_atoi:
    push {lr}
    push {r4-r11}

    mov r2, #0x0 //; init string length counter
    mov r4, #0x0 //; end state counter value
    mov r6, #1 //; Current idx to multiply by
    mov r7, #10


_str_length_loop:
    ldrb r8, [r0] //; load a byte out of the string
                //; dereferencing r0
    cmp r8, #0xa //; check equals \n
    beq _count
    add r0, r0, #1 //; increment pointer
    add r2, r2, #1
    b _str_length_loop

_count:
    sub r0, r0, #1

    ldrb r8, [r0] //; the first number in the string
    sub r8, r8, #0x30 //; 0x30 = '0'
    mul r4, r8, r6 //; current place times number
    mov r8, r4 
    mul r4, r6, r7 //; increment the placeholder
    mov r6, r4
    add r5, r5, r8 //; add current number to counter
    sub r2, r2, #1 //; decrement length check for end
    cmp r2, #0x0 //; If counter == 0, we leave
    beq _leave
    b _count

_leave:
    mov r0, r5

    pop {r4-r11}
    pop {pc}

//; r0 = integer to display ( < 10000)
int_string:
    push {lr}
    push {r4-r11}
    mov r2, #0x0 //; lenght counter
    mov r3, #1000 //; Current place. Placeholder for 1000
    mov r7, #10 //; place divisor

_loop:
    mov r4, #0x0
    udiv r4, r0, r3 ;// r4 = r0/r3

    add r4, r4, #0x30

    ldr r5, =sum //; Load to r5 the addr of the sum
    add r5, r5, r2
    strb r4, [r5]
    add r2, r2, #1

    sub r4, r4, #0x30 //; go back down to the number
    mul r6, r4, r3
    sub r0, r0, r6

    udiv r6, r3, r7
    mov r3, r6
    cmp r3, #0
    beq _leave_int
    b _loop

_leave_int:
    mov r4, #0xa
    ldr r5, =sum
    add r5, r5, r2
    add r5, r5, #1
    strb r4, [r5]
    pop {r4-r11}
    pop {pc}

display:
    push {lr}
    push {r4-r11}

    mov r7, #0x4
    mov r0, #0x1
    ldr r1, =sum
    mov r2, #0x8
    svc 0x0
    pop {r4-r11}
    pop {pc}

_start:
    ldr r0, =m_arg1
    mov r1, #0x6
    bl read_user_input

    ldr r0, =m_arg2
    mov r1, #0x6
    bl read_user_input

    //; convert input into a number
    ldr r0, =m_arg1
    bl convert_atoi
    mov r4, r0

    ldr r0, =m_arg2
    bl convert_atoi
    mov r5, r0

    //; add the 2 inputs
    add r0, r4, r5 
    bl int_string

    //; display
    bl display

    mov r0, #0x0
    mov r7, #0x1
    //; mov r0, #13
    svc 0x0

.section .data
    m_arg1:
        .skip 8
    m_arg2:
        .skip 8
    sum:
        .skip 8
